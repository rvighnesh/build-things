#! /bin/bash

set -euxo pipefail

amazon-linux-extras install docker -y

dockerd

docker run -d -e POSTGRES_PASSWORD=example -e PGDATA=/var/lib/postgresql/data/pgdata postgres

# yum update -y
# amazon-linux-extras enable postgresql14
# yum install postgresql-server -y
# export PGDATA=/data/postgres
# # postgresql-setup --initdb
# systemctl start postgresql
# systemctl enable postgresql
# systemctl status postgresql

# psql -c "ALTER USER postgres PASSWORD 'example';"
